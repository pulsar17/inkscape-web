#
# Copyright 2018, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Track the visiting people in the forum.
"""

import socket
import struct

from collections import OrderedDict
from datetime import timedelta

from django.conf import settings
from django.core.cache import caches
from django.core.handlers.wsgi import WSGIRequest
from django.utils.timezone import now

from person.models import User

# Age to count visits if users are really still here in minutes
VISITOR_AGE = timedelta(seconds=getattr(settings, 'FORUM_VISITOR_AGE', 60) * 60)

class RecentUsersMiddleware:
    """
    Record a list of users who have visited the forums recently.
    """

    def __init__(self, handler=None):
        self.get_response = handler
        self.cache = caches[settings.CACHE_MIDDLEWARE_ALIAS]

    def __call__(self, request):
        return self.get_response(request)

    def process_template_response(self, request, response):
        """Add visiting user data into context"""
        if hasattr(response, 'context_data') and isinstance(request, WSGIRequest):
            if self.cache is not None:
                visitors = self.visitors(request)
                response.context_data['visitors'] = visitors
                response.context_data['lurkers'] = sum([1 for v in visitors.values() if 'username' not in v])
        return response

    def visitors(self, request):
        ip_pk = self.get_ip_pk(request)
        if hasattr(request, 'user') and request.user.is_authenticated:
            if getattr(settings, 'FORUM_DEBUG_ONLINE', False):
                # Add some test users when in debug mode
                user_qset = User.objects.exclude(photo__isnull=True).exclude(photo='')
                for user in user_qset.order_by('?')[:3]:
                    self.set_visitor(user, now())

            return self.set_visitor(request.user, now(), ip_pk)

        if ip_pk and str(request.path_info).startswith('/forum'):
            return self.set_lurker(ip_pk, now())
        return self.get_visitors()

    def set_visitor(self, user, at_time, ip_pk=None):
        """Record a user as visiting at this time"""
        visitors = self.get_visitors()
        visitors.pop(user.pk, None)
        visitors[user.pk] = {
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'photo_url': user.photo_url(),
            'last_seen': at_time,
            'ip_pk': ip_pk,
        }
        if ip_pk and ip_pk in visitors:
            visitors.pop(ip_pk) # Ignore 
        self.cache.set('visitors', list(visitors.items()))
        return visitors

    def set_lurker(self, ip_pk, at_time):
        """Set a visit from an anonymous user"""
        visitors = self.get_visitors()
        visitors.pop(ip_pk, None)
        for visitor in visitors.values():
            if visitor.get('ip_pk', None) == ip_pk:
                # Don't record echos of users who log off
                return visitors
        visitors[ip_pk] = {
            'ip_pk': ip_pk,
            'last_seen': at_time,
        }
        self.cache.set('visitors', list(visitors.items()))
        return visitors

    def get_visitors(self):
        """Return a sorteded list of visitors"""
        return OrderedDict((pk, visitor)
                           for pk, visitor in self.cache.get('visitors', [])
                           if now() - visitor['last_seen'] < VISITOR_AGE)

    def get_ip_pk(self, request):
        """Get remote ip address"""
        if getattr(settings, 'FORUM_DEBUG_ONLINE', False):
            import random
            return random.randint(1, 3000)
        try:
            remote_ip = request.META.get("REMOTE_ADDR", "")
            forwarded_ip = request.META.get("HTTP_X_FORWARDED_FOR", "")
            ip = remote_ip if not forwarded_ip else forwarded_ip
            return struct.unpack("!L", socket.inet_aton(ip))[0]
        except IOError:
            return None

