#
# Copyright 2022, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Methods for getting budget statistics
"""

from collections import OrderedDict

from django.db.models import F, Count
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from stats.utils import get_stats
from stats.base import StatisticBase, DateCountBase

from person.models import User
from .models import Budget, EntryValue, BudgetCategory

class BudgetTotal(DateCountBase):
    """A bar chart of budget totals"""
    default_template = 'stats/date_dynamic.html'
    title = _("Budget Totals")

    def filter_data(self, qset):
        return {
            'raw': qset.values_list('point', 'total'),
            'options': self.chart_options,
        }

    def get_data(self):
        return self.options['budget'].entries.order_by('point')


class BudgetIncome(DateCountBase):
    title = _("Budget Income")
    chart_type = 'bar'

    def filter_data(self, qset):
        cats = OrderedDict()
        labels = []
        for cat, val, dtm in qset.values_list('category_id', 'value', 'entry__point'):
            cat = BudgetCategory.objects.get(pk=cat)
            if dtm not in labels:
                labels.append(dtm)
            index = labels.index(dtm)

            if cat not in cats:
                cats[cat] = []
            cats[cat] += [0] * (len(labels) - len(cats[cat]))
            cats[cat][index] = val

        return {
            'labels': self.space_labels(labels),
            'series': [(cat, values) for cat, values in cats.items()],
            'chart': self.chart_type,
            'options': self.chart_options,
        }

    def get_data(self):
        return EntryValue.objects.filter(
            entry__budget=self.options['budget'],
            category__is_previous=False,
            category__is_income=True)

class BudgetExpense(BudgetIncome):
    title = _("Budget Expenses");

    def get_data(self):
        return EntryValue.objects.filter(
            entry__budget=self.options['budget'],
            category__is_previous=False,
            category__is_income=False)

